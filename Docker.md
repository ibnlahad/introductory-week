# Docker

## Para que sirve Docker?

Docker te permite **construir** , **distribuir** y **ejecutar** cualquier aplicacion en cualquier lado.

### Virtualizacion

Permite en resolver en simultaneo los tres grandes problemas del desarrollo de software.
Docker quita el termino de virtualizacion debido a los problemas que esta conlleva. Por lo cual la solucion que plantea esta es: Contenedores

### Contenedores

Los contenedores son entidades en las cuales se puede cpnstruir y desplegar software. Las ventajas de los contenedores son los siguientes:

- Flexibles:
  Puedes ejecutar cualquier aplicacion en el contenedor
- Livianos
  Todos los contenedores son livianos debido a que estos reutilizan el kernel de la maquina que se este utilizando, haciendo de esto la posibilidad de solo empaquetar nuestro codigo.
- Portables
  Estan diseñados para ejecutarse en cualquier maquina.
- Bajo acoplamiento:
  Lo que haya en un contenedor no afecta lo que haya en otros.
- Escalables
  Son muy daciles de mejorar y darles un upgrade.
- Seguros
  Se aseguran de acceder a lo que este se le define.

## Arquitectura Docker

La arquitectura de Docker se define en 3 grandes areas.

- Server o Docker Daemon  
  Este se refiere al centro de docker, el corazón que gracias a el, podemos comunicarnos con los servicios de docker.
- Rest API
  Este hace referencia a la visualizacion de servicios docker de forma grafica, como una simple REST API
- Cliente de Docker
  Con este podemos comunicarnos con el corazon de Docker. Este por defecto es la linea de comandos.

### Principios de Docker

- Contenedores
  Es la razón de ser de Docker, es donde podemos encapsular nuestras imagenes para llevarlas a otra computadora, o servidor, etc.
- Imagenes
  Son las encapsulaciones de x contenedor. Podemos correr nuestra aplicación en Java por medio de una imagen, podemos utilizar Ubuntu para correr nuestro proyecto, etc.
- Volumenes de Datos
  Podemos acceder con seguridad al sistema de archivos de nuestra máquina.
- Redes
- Son las que permiten la comunicación entre contenedores.

## Primeros pasos en Docker

Comandos Utiles:
**_docker run "Nombre de un contenedor"_** Arranca un contenedor segun su nombre

**_docker run --name contenedor_test ubuntu_**
Arranca un contenedor asignandole un nombre

## Conceptos Fundamentales

### Contenedores

Se puede definir como una maquina virtual liviana. Es una agrupacion de procesos que corren nativamente en la maquina pero que estan aislados del resto de sistemas. Es una agrupacion logica
